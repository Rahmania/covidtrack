package com.example.covidtrack.Retrofit;

import android.content.Context;

import com.example.covidtrack.R;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientService {

    private static final String BaseURL            = "https://api.covid19api.com/";
    private  static final String GermanCovidTrack = "https://www.trackcorona.live/api/countries/";

    private static Retrofit retrofit = null;
    private static Retrofit retrofit2 = null;
    private static Retrofit retrofit3 = null;


    public static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BaseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(configureTimeouts())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClient2(Context context){
        if (retrofit2 == null){
            retrofit2 = new Retrofit.Builder()
                    .baseUrl(context.getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(configureTimeouts())
                    .build();
        }
        return retrofit2;
    }

    public static Retrofit getClient3(){
        if (retrofit3 == null){
            retrofit3 = new Retrofit.Builder()
                    .baseUrl(GermanCovidTrack)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(configureTimeouts())
                    .build();
        }
        return retrofit3;
    }

    public static OkHttpClient configureTimeouts() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS) // Set your timeout duration here.
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build();
        return okHttpClient;
    }

}
