package com.example.covidtrack.Dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covidtrack.BLE.BLEDevice;
import com.example.covidtrack.BLE.BroadcastReceiver;
import com.example.covidtrack.BLE.ListBLEDevices;
import com.example.covidtrack.CentralizeClass.StorageClass;
import com.example.covidtrack.Methods.AllMethods;
import com.example.covidtrack.R;
import com.example.covidtrack.BLE.ScanningBLE;
import com.example.covidtrack.User.AccountActivity;
import com.example.covidtrack.User.LoginActivity;
import com.example.covidtrack.BLE.Utils;
import com.google.android.material.navigation.NavigationView;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener, View.OnClickListener {

    public static final int REQUEST_ENABLE_BT = 1;
    private HashMap<String, BLEDevice> mBTDevicesHashMap;
    private ArrayList<BLEDevice> mBTDevicesArrayList;
    private ListBLEDevices adapter;

    private BroadcastReceiver mBTStateUpdateReceiver;
    private ScanningBLE mBTLeScanner;
    TextView tv_scan;


    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tv_newConfirmed_count) TextView tv_newConfirmed_count;
    @BindView(R.id.tv_TotalConfirmed_count) TextView tv_TotalConfirmed_count;
    @BindView(R.id.tv_NewDeaths_count) TextView tv_NewDeaths_count;
    @BindView(R.id.tv_TotalDeaths_count) TextView tv_ToatalDeaths_count;
    @BindView(R.id.tv_NewRecovered_count) TextView tv_NewRecovered_count;
    @BindView(R.id.tv_TotalRecovered_count) TextView tv_TotalRecovered_count;
    @BindView(R.id.tv_GermanyConfirmedNumbers) TextView GConfirmed;
    @BindView(R.id.tv_GermanyConfirmedrecovered) TextView GRecovered;
    @BindView(R.id.tv_GermanyConfirmedDeaths) TextView GDeaths;
    @BindView(R.id.buttonCheckHealth) Button CheckHealthStatus;
    @BindView(R.id.spinner)Spinner spinner;
    @BindView(R.id.devicesList) ListView lvDeviceList;
//    @BindView(R.id.buttonBluetooth)Button startBluetooth;

    String[] radius ={"Select Radius",".500","1","2"};



    ActionBarDrawerToggle toggle;
    double latitude;
    double longitude;
    String BTaddress;
    String Locationradius;

    AllMethods allMethods = new AllMethods();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        tv_scan = findViewById(R.id.tv_scan);
        spinner.setOnItemSelectedListener(this);

        DrawerLayout drawerLayout =findViewById(R.id.drawer_layout);
        NavigationView navigationView =findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle
                (
                        this,
                        drawerLayout,
                        toolbar,
                        R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close
                );
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            Toast.makeText(getApplicationContext(),"BLE Not Supported",Toast.LENGTH_LONG).show();
            finish();
        }else{
            BTaddress=getBTaddress();
        }
        mBTStateUpdateReceiver = new BroadcastReceiver(getApplicationContext());
        mBTLeScanner = new ScanningBLE(DashboardActivity.this, 10000, -750);

        mBTDevicesHashMap = new HashMap<>();
        mBTDevicesArrayList = new ArrayList<>();
        adapter = new ListBLEDevices(this, R.layout.bluetoothitem, mBTDevicesArrayList);

        lvDeviceList.setAdapter(adapter);

        findViewById(R.id.tv_scan).setOnClickListener(this);




        PermissionListener pmListener =new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Toast.makeText(DashboardActivity.this, "Permission Approved", Toast.LENGTH_SHORT).show();

                SmartLocation.with(DashboardActivity.this).location()
                        .start(new OnLocationUpdatedListener() {
                            @Override
                            public void onLocationUpdated(Location location) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                allMethods.updateLocation(DashboardActivity.this, latitude,longitude, BTaddress);
                                Log.d("lat", String.valueOf(latitude));
                                Log.d("lng", String.valueOf(longitude));
                            }
                        });

            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(DashboardActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        TedPermission.with(DashboardActivity.this)
                .setPermissionListener(pmListener)
                .setDeniedMessage("Please turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .check();

        CheckHealthStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this,CheckHealthActivity.class);
                startActivity(intent);
            }
        });



        allMethods.geoFencing(this, latitude,longitude);
        allMethods.SocialDistance(this, latitude,longitude);
        allMethods.getCovidSummary(tv_newConfirmed_count,tv_TotalConfirmed_count,tv_NewDeaths_count,tv_ToatalDeaths_count,
                                    tv_NewRecovered_count,tv_TotalRecovered_count);
        allMethods.GermanyStats(DashboardActivity.this,GConfirmed,GRecovered,GDeaths);




        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,radius);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);


    }

    private String getBTaddress(){
        BTaddress =android.provider.Settings.Secure.getString(this.getContentResolver(),"bluetooth_address");
        return BTaddress;
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(mBTStateUpdateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        stopScan();
    }
    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(mBTStateUpdateReceiver);
        stopScan();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Utils.toast(getApplicationContext(), "Thank you for turning on Bluetooth");
            } else if (resultCode == RESULT_CANCELED) {
                Utils.toast(getApplicationContext(), "Please turn on Bluetooth");
            }
        }
    }








    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(DashboardActivity.this, DashboardActivity.class);
            startActivity(intent);

        } else if (id == R.id.menuAccount) {
            Intent intent = new Intent(DashboardActivity.this, AccountActivity.class );
            startActivity(intent);
        }else if (id == R.id.menu_about) {
            Intent intent = new Intent(DashboardActivity.this, AboutActivity.class);
            startActivity(intent);

        } else if (id == R.id.logout) {

            LogoutPopup();

        }

//        }else if(id  == R.id.nav_address){
//            Intent intent = new Intent(DashboardActivity.this, Address_Activity.class);
//            intent.putExtra("userID", uid);
//            intent.putExtra("uname", nav_name.getText().toString());
//            intent.putExtra("phone", nav_phone.getText().toString());
//            intent.putExtra("email", nav_email.getText().toString());
//            startActivity(intent);
//
//        }else if(id == R.id.nav_logout){
//            SharedPreferences preferences =getSharedPreferences("asapPrefs", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = preferences.edit();
//            editor.clear();
//            editor.apply();
//            finish();
//            SharedPreferences sharedPreferences= getSharedPreferences("profileurl", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor1 = sharedPreferences.edit();
//            editor1.clear();
//            editor1.apply();
//            finish();
//
//            Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
//            intent.putExtra("finish", true); // if you are checking for this in your other Activities
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
//                    Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finish();
//
//        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void LogoutPopup() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setMessage("");
        builder.setIcon(R.drawable.error);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               // Singleton.getInstance().removeUser(DashboardActivity.this);
                StorageClass.getInstance().DeleteUser(DashboardActivity.this);
                Intent  intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(intent);
                DashboardActivity.this.finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.show();

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.tv_scan) {
            if (!mBTLeScanner.isScanning()) {
                startScan();
            } else {
                stopScan();
            }
        }
    }

    public void addDevice(BluetoothDevice device, int rssi) {

        String address = device.getAddress();
        if (!mBTDevicesHashMap.containsKey(address)) {
            BLEDevice btleDevice = new BLEDevice(device);
            btleDevice.setRSSI(rssi);

            mBTDevicesHashMap.put(address, btleDevice);
            mBTDevicesArrayList.add(btleDevice);


        }
        else {
            mBTDevicesHashMap.get(address).setRSSI(rssi);
        }

        adapter.notifyDataSetChanged();
    }
    public void startScan(){
        tv_scan.setText("Scanning...");

        mBTDevicesArrayList.clear();
        mBTDevicesHashMap.clear();
        adapter.notifyDataSetChanged();
        mBTLeScanner.start();
    }

    public void stopScan() {
        tv_scan.setText("Do you want to scan Again?");

        mBTLeScanner.stop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            DashboardActivity.this.finish();

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getApplicationContext(),radius[i] , Toast.LENGTH_LONG).show();
        allMethods.radius(DashboardActivity.this, radius[i],latitude,longitude);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }



}