package com.example.covidtrack.User;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covidtrack.Models.RegisterModel;
import com.example.covidtrack.R;
import com.example.covidtrack.Retrofit.ApiClientService;
import com.example.covidtrack.Retrofit.Interface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {


    @BindView(R.id.et_Fullname) EditText name;
    @BindView(R.id.et_email) EditText email;
    @BindView(R.id.et_password) EditText password;
    @BindView(R.id.et_phone) EditText phone;
    @BindView(R.id.buttonsignUp) Button signUp;
    @BindView(R.id.tv_login)TextView login;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });


    }

    public void register(){

        Interface apiInterface = ApiClientService.getClient2(RegisterActivity.this).create(Interface.class);
        RegisterModel registerModel = new RegisterModel();
        registerModel.setName(name.getText().toString());
        registerModel.setEmail(email.getText().toString());
        registerModel.setPhone(phone.getText().toString());
        registerModel.setPassword(password.getText().toString());
        registerModel.setCurrentLat("");
        registerModel.setCurrentLong("");
        registerModel.setBluetoothAddress("");

        Call<ResponseBody> call = apiInterface.userRegistration(registerModel);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    JSONArray jsonArray = new JSONArray(res);
                    String message =jsonArray.getJSONObject(0).getString("Msg");
                    if (message.equalsIgnoreCase("Success")){
                        intent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(intent);
                        RegisterActivity.this.finish();
                    }
                    Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
}