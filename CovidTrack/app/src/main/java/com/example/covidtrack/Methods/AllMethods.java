package com.example.covidtrack.Methods;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.example.covidtrack.CentralizeClass.StorageClass;
import com.example.covidtrack.Dashboard.DashboardActivity;
import com.example.covidtrack.Models.RadiusModel;
import com.example.covidtrack.Models.SocialDistanceModel;
import com.example.covidtrack.Models.UpdateHealthStatusModel;
import com.example.covidtrack.Models.UpdateLocationModel;
import com.example.covidtrack.R;
import com.example.covidtrack.Retrofit.ApiClientService;
import com.example.covidtrack.Retrofit.Interface;
import com.google.android.gms.location.Geofence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.DoubleAccumulator;

import io.nlopez.smartlocation.OnGeofencingTransitionListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.geofencing.model.GeofenceModel;
import io.nlopez.smartlocation.geofencing.utils.TransitionGeofence;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllMethods {


    public void SocialDistance(Context context , Double latitude, Double longitude){

        Date date = new Date();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat gettime = new SimpleDateFormat("HH:mm");
        String time = gettime.format(date);
        Log.d("datetry",time);

        Interface apiInterface = ApiClientService.getClient2(context).create(Interface.class);
        SocialDistanceModel socialDistanceModel = new SocialDistanceModel();
        socialDistanceModel.setLat(""+latitude);
        socialDistanceModel.setLong(""+longitude);
        socialDistanceModel.setTime(time);
        Call<ResponseBody> call = apiInterface.socialDistance(socialDistanceModel);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res=  response.body().string();
                    JSONArray jsonArray = new JSONArray(res);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String msg = jsonObject.getString("Msg");
                    if (msg.equalsIgnoreCase("Distance Violated")){
                        AlertDialog.Builder  builder = new AlertDialog.Builder(context);
                        builder.setTitle(msg);
                        builder.setMessage("Check your health again");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        builder.show();
                    }
                    //Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });



    }

    public void geoFencing(Context context, Double lat, Double lng){


        GeofenceModel mestalla  = new GeofenceModel.Builder("id_mestalla")
                .setTransition(Geofence.GEOFENCE_TRANSITION_ENTER)
                .setLatitude(lat)
                .setLongitude(lng)
                .setRadius(10)
                .build();
        GeofenceModel cuenca = new GeofenceModel.Builder("id_cuenca")
                .setTransition(Geofence.GEOFENCE_TRANSITION_EXIT)
                .setLatitude(lat)
                .setLongitude(lng)
                .setRadius(20)
                .build();

        SmartLocation.with(context).geofencing()
                .add(mestalla)
                .add(cuenca)
                .remove("already_existing_geofence_id")
                .start(new OnGeofencingTransitionListener() {
                    @Override
                    public void onGeofenceTransition(TransitionGeofence transitionGeofence) {

                        Toast.makeText(context,"fencing",Toast.LENGTH_LONG).show();

                    }
                });

    }

    public void getCovidSummary(TextView tv_newConfirmed_count,
                                 TextView tv_TotalConfirmed_count,
                                 TextView tv_NewDeaths_count,
                                 TextView tv_ToatalDeaths_count,
                                 TextView tv_NewRecovered_count,
                                 TextView tv_TotalRecovered_count){

        Interface apiInterface = ApiClientService.getClient().create(Interface.class);
        Call<ResponseBody> call = apiInterface.covidSummary();
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res= response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    JSONObject globalObject = jsonObject.getJSONObject("Global");

                    tv_newConfirmed_count.setText("New Confirmed      : "+globalObject.getString("NewConfirmed"));
                    tv_TotalConfirmed_count.setText("Total Confirmed     : "+globalObject.getString("TotalConfirmed"));
                    tv_NewDeaths_count.setText("New Deaths            : "+globalObject.getString("NewDeaths"));
                    tv_ToatalDeaths_count.setText("Total Deaths           : "+globalObject.getString("TotalDeaths"));
                    tv_NewRecovered_count.setText("New Recovered      : "+globalObject.getString("NewRecovered"));
                    tv_TotalRecovered_count.setText("Total Recovered     : "+globalObject.getString("TotalRecovered"));

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }



    public void updateLocation(Context context , Double latitude, Double longitude, String BTaddress){

        String id= StorageClass.getInstance().getUserID(context);
        Interface apiInterFace = ApiClientService.getClient2(context).create(Interface.class);
        UpdateLocationModel updateLocationModel = new UpdateLocationModel();
        updateLocationModel.setUuid(id);
        updateLocationModel.setCurrentLat(""+latitude);
        updateLocationModel.setCurrentLong(""+longitude);

        if (BTaddress!=null){
            updateLocationModel.setBluetoothAddress(BTaddress);
        }
        else{
            updateLocationModel.setBluetoothAddress("0.0.0.0.0.0.0.0");
        }


        Call<ResponseBody> c = apiInterFace.updateLocation(updateLocationModel);
        c.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String r = response.body().string();
                    JSONArray jsonArray = new JSONArray(r);
                    String Message = jsonArray.getJSONObject(0).getString("Msg");
                    Toast.makeText(context,Message,Toast.LENGTH_LONG).show();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });





    }


    public void GermanyStats(Context context,TextView GConfirmed,TextView GRecovered,TextView GDeaths){

        Interface apiInterface = ApiClientService.getClient3().create(Interface.class);
        Call<ResponseBody> c = apiInterface.GermanStats();
        c.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String responseString = response.body().string();
                    JSONObject jsonObject = new JSONObject(responseString);
                    JSONArray dataArray = jsonObject.getJSONArray("data");
                    GConfirmed.setText(dataArray.getJSONObject(0).getString("confirmed"));
                    GDeaths.setText(dataArray.getJSONObject(0).getString("dead"));
                    GRecovered.setText(dataArray.getJSONObject(0).getString("recovered"));
//                    String location = dataArray.getJSONObject(0).getString("location");
//                    Toast.makeText(context,location,Toast.LENGTH_LONG).show();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });




    }


    public void radius(Context context, String distance,Double lat,Double lng){
        Interface apiInterFace = ApiClientService.getClient2(context).create(Interface.class);
        RadiusModel radiusModel = new RadiusModel();
        radiusModel.setDistance(distance);
        radiusModel.setLat(""+lat);
        radiusModel.setLong(""+lng);
        Call<ResponseBody> c = apiInterFace.radius(radiusModel);
        c.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String responseString = response.body().string();
                    JSONArray jsonArray = new JSONArray(responseString);
                    String TotalUsers = String.valueOf(jsonArray.length());
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custom_dialogbox);
                    TextView appUsers = (TextView) dialog.findViewById(R.id.tv_appUsers);
                    TextView Husers = (TextView) dialog.findViewById(R.id.tv_healthyUsers);
                    TextView NCusers = (TextView) dialog.findViewById(R.id.tv_notCheckedUsers);
                    TextView Rusers = (TextView) dialog.findViewById(R.id.tv_riskyUsers);
                    Button dialogButton = dialog.findViewById(R.id.ButtonOK);
                    appUsers.setText(TotalUsers+" are Active Users");

                    String healthy = null;
                    int hCount = 0;
                    int NCcount = 0;
                    for(int i=0; i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        healthy = jsonObject.getString("HealthStatus");

                        if (healthy.equalsIgnoreCase("Healthy")){
                            healthy = String.valueOf(hCount +1);

                        }else if (healthy.equalsIgnoreCase("Not checked")){
                            NCcount = NCcount +1;

                        }
                       // Log.d("healthstatus",healtyStatus);
                    }
                    Husers.setText(healthy+" Healthy people");
                    if (NCcount != 0){
                        NCusers.setText(NCcount+" not checked");
                    }

                    Rusers.setText("0" + "People Unsafe");
//                    Log.d("healtyCount",""+healtyCount);
//                    Log.d("notchecked",""+notCheckedCount);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }







}
