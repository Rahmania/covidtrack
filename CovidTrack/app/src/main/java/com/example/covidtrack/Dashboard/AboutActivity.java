package com.example.covidtrack.Dashboard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.covidtrack.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}