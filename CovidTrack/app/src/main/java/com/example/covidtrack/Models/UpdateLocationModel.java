package com.example.covidtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateLocationModel {

    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("currentLat")
    @Expose
    private String currentLat;
    @SerializedName("currentLong")
    @Expose
    private String currentLong;
    @SerializedName("bluetoothAddress")
    @Expose
    private String bluetoothAddress;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(String currentLat) {
        this.currentLat = currentLat;
    }

    public String getCurrentLong() {
        return currentLong;
    }

    public void setCurrentLong(String currentLong) {
        this.currentLong = currentLong;
    }

    public String getBluetoothAddress() {
        return bluetoothAddress;
    }

    public void setBluetoothAddress(String bluetoothAddress) {
        this.bluetoothAddress = bluetoothAddress;
    }

}
