package com.example.covidtrack.Dashboard;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covidtrack.CentralizeClass.StorageClass;
import com.example.covidtrack.Methods.AllMethods;
import com.example.covidtrack.Models.HealthStatusModel;
import com.example.covidtrack.Models.UpdateHealthStatusModel;
import com.example.covidtrack.R;
import com.example.covidtrack.Retrofit.ApiClientService;
import com.example.covidtrack.Retrofit.Interface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckHealthActivity extends AppCompatActivity {

    @BindView(R.id.checkbox_cough) CheckBox cough;
    @BindView(R.id.checkbox_fever) CheckBox fever;
    @BindView(R.id.checkbox_diarrhea) CheckBox diarrhea;
    @BindView(R.id.checkbox_breathing) CheckBox breathing;
    @BindView(R.id.checkbox_smell) CheckBox smell;
    @BindView(R.id.checkbox_taste) CheckBox taste;
    @BindView(R.id.btn_checkHealthStatus)Button checkHealthStatus;
    @BindView(R.id.btnReset)Button Reset;
    @BindView(R.id.iv_safeImage)ImageView iv_safe;
    @BindView(R.id.iv_unsafeImage)ImageView iv_Unsafe;
    @BindView(R.id.tv_youaresafe) TextView tv_safe;
    @BindView(R.id.tv_youareUnsafe) TextView tv_Unsafe;

    int count = 0;
    String status ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_health);

        ButterKnife.bind(this);

        checkHealthStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkBoxClicks();
                if (count>3){
//                    Toast.makeText(getApplicationContext(),"Risky"+count,Toast.LENGTH_LONG).show();

                    count =0 ;

//                    builder.setMessage("Risky");
                    showUnSafeAlert();


                }
                else{
                    showSafeAlert();

//                    Toast.makeText(getApplicationContext(),"You are safe"+count,Toast.LENGTH_LONG).show();
                }
//                Log.d("count", String.valueOf(count));
//                Toast.makeText(getApplicationContext(),""+count,Toast.LENGTH_LONG).show();

            }
        });

        Reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetCheckBox();
            }
        });


        CheckHealthStatus();
    }

    public void checkBoxClicks(){
        if (cough.isChecked()){
            count  = count +1;
        }
        if (fever.isChecked()){
            count  = count +1;
        }
        if (diarrhea.isChecked()){
            count  = count +1;
        }
        if (breathing.isChecked()){
            count  = count +1;
        }
        if (taste.isChecked()){
            count  = count +1;
        }
        if (smell.isChecked()) {
            count = count + 1;
        }

    }
    public void resetCheckBox(){
        if (cough.isChecked()){
            cough.setChecked(false);
        }
        if (fever.isChecked()){
            fever.setChecked(false);
        }
        if (diarrhea.isChecked()){
            diarrhea.setChecked(false);
        }
        if (breathing.isChecked()){
            breathing.setChecked(false);
        }
        if (taste.isChecked()){
            taste.setChecked(false);
        }
        if (smell.isChecked()) {
            smell.setChecked(false);
        }
    }

    public void showSafeAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(CheckHealthActivity.this);
        builder.setIcon(R.drawable.error);
        builder.setTitle("Health Status");
        builder.setMessage("Your are safe");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                status = "Healthy";
                updateHealthStatus();
                CheckHealthStatus();
                resetCheckBox();

            }
        });
        builder.create();
        builder.show();
    }


    public void showUnSafeAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(CheckHealthActivity.this);
        builder.setIcon(R.drawable.error);
        builder.setTitle("Health Status");
        builder.setMessage("Your are not safe");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                status = "Risky";
                updateHealthStatus();
                CheckHealthStatus();
                resetCheckBox();

            }
        });
        builder.create();
        builder.show();
    }



    public void updateHealthStatus(){

        String uid = StorageClass.getInstance().getUserID(this);

        Interface apiIterface = ApiClientService.getClient2(this).create(Interface.class);
        UpdateHealthStatusModel updateHealthStatusModel = new UpdateHealthStatusModel();
        updateHealthStatusModel.setUserId(uid);
        updateHealthStatusModel.setHealthStatus(status);
        Call<ResponseBody> call =apiIterface.updateHealthStatus(updateHealthStatusModel);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();
                    JSONArray jsonArray = new JSONArray(res);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String healthStatus = jsonObject.getString("HealthStatus");

                    Toast.makeText(getApplicationContext(),healthStatus,Toast.LENGTH_LONG).show();


                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });





    }

    public void CheckHealthStatus(){
        String uid = StorageClass.getInstance().getUserID(this);

        Interface apiInterface = ApiClientService.getClient2(this).create(Interface.class);
        HealthStatusModel healthStatusModel = new HealthStatusModel();
        healthStatusModel.setUserId(uid);
        Call<ResponseBody> call = apiInterface.HealthStatus(healthStatusModel);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res=  response.body().string();
                    JSONArray jsonArray = new JSONArray(res);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String status = jsonObject.getString("HealthStatus");
                    if (status.equalsIgnoreCase("Healthy")){
                        tv_safe.setText("You are Safe");

                    }else if(status.equalsIgnoreCase("Risky")) {
                        tv_Unsafe.setText("You are Unsafe");
                        iv_safe.setVisibility(View.GONE);
                        tv_safe.setVisibility(View.GONE);
                        tv_Unsafe.setVisibility(View.VISIBLE);
                        iv_Unsafe.setVisibility(View.VISIBLE);


                    }else{
                        tv_safe.setText("Not checked");
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

}