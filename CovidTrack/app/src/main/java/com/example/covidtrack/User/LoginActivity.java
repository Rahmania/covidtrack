package com.example.covidtrack.User;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covidtrack.CentralizeClass.StorageClass;
import com.example.covidtrack.Dashboard.DashboardActivity;
import com.example.covidtrack.Models.SignInResponseModel;
import com.example.covidtrack.Models.SigninModel;
import com.example.covidtrack.R;
import com.example.covidtrack.Retrofit.ApiClientService;
import com.example.covidtrack.Retrofit.Interface;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.tv_signUp) TextView tv_SignUp;
    @BindView(R.id.button_Login) Button button_login;
    @BindView(R.id.et_email) EditText email;
    @BindView(R.id.et_password) EditText password;

    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);


        StorageClass.getInstance().getUserDetails(LoginActivity.this);
        String value =StorageClass.getInstance().getUserID(LoginActivity.this);
        if (value.equalsIgnoreCase("")){

        }else{
            Intent intent = new Intent(LoginActivity.this,DashboardActivity.class);
            startActivity(intent);
            LoginActivity.this.finish();
        }


        tv_SignUp.setOnClickListener(this);
        button_login.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_signUp){
            intent = new Intent(LoginActivity.this,RegisterActivity.class);
            startActivity(intent);
        }
        if (view.getId()== R.id.button_Login){
            if (email.getText().toString().equalsIgnoreCase("")){
                Toast.makeText(getApplicationContext(),"All Fields are required",Toast.LENGTH_LONG).show();
                return;
            }
            if (password.getText().toString().equalsIgnoreCase("")){
                Toast.makeText(getApplicationContext(),"All Fields are required",Toast.LENGTH_LONG).show();
                return;
            }

            LoginMethod();
        }
    }


    private void LoginMethod(){
        Interface apiIterface = ApiClientService.getClient2(LoginActivity.this).create(Interface.class);
        SigninModel signinModel = new SigninModel();
        signinModel.setEmail(email.getText().toString());
        signinModel.setPassword(password.getText().toString());

        Call<List<SignInResponseModel>> c = apiIterface.userLogin(signinModel);
        c.enqueue(new Callback<List<SignInResponseModel>>() {
            @Override
            public void onResponse(Call<List<SignInResponseModel>> call, Response<List<SignInResponseModel>> response) {
                List<SignInResponseModel> sList  = response.body();

                if (sList.size()>0){

                    SignInResponseModel SigninResponse = sList.get(0);
                    String Message = SigninResponse.getMsg();
                    if (SigninResponse.getMsg().equalsIgnoreCase("Success")){

                        StorageClass.getInstance().saveDetails(LoginActivity.this, SigninResponse);
                        intent = new Intent(LoginActivity.this, DashboardActivity.class);
                        startActivity(intent);
                        LoginActivity.this.finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<SignInResponseModel>> call, Throwable t) {

            }
        });

    }

}