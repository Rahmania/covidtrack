package com.example.covidtrack.Retrofit;

import com.example.covidtrack.Models.HealthStatusModel;
import com.example.covidtrack.Models.RadiusModel;
import com.example.covidtrack.Models.SignInResponseModel;
import com.example.covidtrack.Models.SigninModel;
import com.example.covidtrack.Models.RegisterModel;
import com.example.covidtrack.Models.SocialDistanceModel;
import com.example.covidtrack.Models.UpdateHealthStatusModel;
import com.example.covidtrack.Models.UpdateLocationModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Interface {

    @GET("summary") Call<ResponseBody>covidSummary();

    @POST("SignUp") Call<ResponseBody> userRegistration(@Body RegisterModel registerModel);

    @POST("Login") Call<List<SignInResponseModel>> userLogin(@Body SigninModel signinModel);

    @POST("UpdateLatLong") Call<ResponseBody> updateLocation(@Body UpdateLocationModel updateLocationModel);

    @POST("checkViolation") Call<ResponseBody> socialDistance(@Body SocialDistanceModel socialDistanceModel);

    @GET("de") Call<ResponseBody> GermanStats();

    @POST("ByLocation") Call<ResponseBody> radius(@Body RadiusModel radiusModel);

    @POST("AddHealthStatus")
    Call<ResponseBody> updateHealthStatus(@Body UpdateHealthStatusModel updateHealthStatusModel);

    @POST("ViewHealthStatus")
    Call<ResponseBody> HealthStatus(@Body HealthStatusModel healthStatusModel );










}
