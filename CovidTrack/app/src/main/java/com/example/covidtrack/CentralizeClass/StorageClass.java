package com.example.covidtrack.CentralizeClass;
import android.content.Context;
import android.content.SharedPreferences;
import com.example.covidtrack.Models.SignInResponseModel;
import com.google.gson.Gson;

public class StorageClass {


    private static  final StorageClass ourInstance = new StorageClass();
    public static final String SHRED_PREF = "UserPrefs";
    public static final String USER_PREF="User";

    public static StorageClass getInstance(){
        return ourInstance;
    }


    public void saveDetails(Context context, SignInResponseModel userObject){
        SharedPreferences.Editor editor =  context.getSharedPreferences(SHRED_PREF, Context.MODE_PRIVATE).edit();
        Gson gson =new Gson();
        String json = gson.toJson(userObject);
        editor.putString(USER_PREF,json);
        editor.apply();
    }

    public SignInResponseModel getUserDetails(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHRED_PREF, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(USER_PREF)) {
            String userString  = sharedPreferences.getString(USER_PREF,"");
            if (userString.equals("")){
                return null;
            }else{
                try{
                    Gson gson =new Gson();
                    SignInResponseModel obj =gson.fromJson(userString, SignInResponseModel.class);
                    return obj;
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }else{
            return null;
        }
        return null;
    }

    public void DeleteUser(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHRED_PREF, Context.MODE_PRIVATE);
        if (sharedPreferences.contains(USER_PREF)) {
            sharedPreferences.edit().remove(USER_PREF).apply();
        }
    }

    public String getUserID(Context context){
       SignInResponseModel userObject = getUserDetails(context);
        if (userObject == null){
            return "";
        }else{
            try{
                String userUUID = userObject.getUuid()  ;
                return userUUID;
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return "";
    }

}
